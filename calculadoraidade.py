from tkinter import *
from tkinter import ttk

#imporantdo tkcalendar
from tkcalendar import Calendar,DateEntry

#imporantdo calculador de data
from dateutil.relativedelta import relativedelta

#importandao datatime
from datetime import date

cor1 = "#000000" #cor preta
cor2 = "#252a33" #cor cinza
cor3 = "#ffffff" #cor branca
cor4 = "#ff8742" #cor laranja

#crindo janela
janela = Tk()
janela.title("Calculadora de idade")
janela.geometry("310x400")

#criando frames
frame_tela = Frame(janela, width=310 ,height=140,padx=0 ,pady=0,relief=FLAT,bg=cor1)
frame_tela.grid(row=0, column=0)

frame_corpo = Frame(janela, width=310 ,height=260,padx=0 ,pady=0,relief=FLAT,bg=cor2)
frame_corpo.grid(row=1, column=0)

#criando labels 
labels_tela = Label(frame_tela, text="CALCULADORA", width=30, height= 1 ,bg=cor1, relief=FLAT,anchor="center",font='Ivy 15 bold',fg=cor3)
labels_tela.place(x=0,y=30)

labels_idade = Label(frame_tela, text="DE IDADE", width=10 , height= 1 ,bg=cor1, relief=FLAT,anchor="center",font='Arial 35 bold',fg=cor4)
labels_idade.place(x=0,y=70)


## funçao que calcula os dias, anos e meses

def calcular():
    inicial = cal_1.get()
    termino = cal_2.get()
    #
    mes_1 , dia_1 , ano_1 = [int(f) for f in inicial.split('/')]
    date_inicial = date(ano_1, mes_1 , dia_1)

    mes_2 , dia_2 , ano_2 = [int(f) for f in termino.split('/')]
    date_termino = date(ano_2, mes_2 , dia_2)

    anos = relativedelta(date_inicial,date_termino,).years
    mes = relativedelta(date_inicial,date_termino,).months
    dias = relativedelta(date_inicial,date_termino,).days
    labels_anos['text'] = anos
    labels_meses['text'] = mes
    labels_dias['text'] = dias




#labels data
labels_data = Label(frame_corpo, text="Data de hoje", width=10 , height= 1 ,bg=cor2, relief=FLAT,font='Ivy 11 bold',fg=cor3, padx=0,pady=0,anchor=NW)
labels_data.place(x=0,y=30)

cal_1 = DateEntry(frame_corpo, width = 13 , bg = "darkblue" , fg = cor3 , borderwidth = 2 , date_pattern = "mm/dd/y", y=2024)
cal_1.place(x=170,y=30)

cal_2 = DateEntry(frame_corpo, width = 13 , bg = "darkblue" , fg = cor3 , borderwidth = 2 , date_pattern = "mm/dd/y", y=2024)
cal_2.place(x=170,y=50)

labels_nascimento = Label(frame_corpo, text="Data nascimento", width=14 , height= 1 ,bg=cor2, relief=FLAT,font='Ivy 11 bold',fg=cor3,padx=0,pady=0,anchor=NW)
labels_nascimento.place(x=0,y=50)

#label dias,anos,meses
labels_anos = Label(frame_corpo, text='--', width=10 , height= 1 ,bg=cor2, relief=FLAT,font='Ivy 25 bold',fg=cor3, padx=0,pady=0,anchor=NW)
labels_anos.place(x=30,y=135)

labels_anos_nome = Label(frame_corpo, text="Anos", width=10 , height= 1 ,bg=cor2, relief=FLAT,font='Ivy 25 bold',fg=cor3, padx=0,pady=0,anchor=NW)
labels_anos_nome.place(x=10,y=180)

labels_meses = Label(frame_corpo, text="--", width=10 , height= 1 ,bg=cor2, relief=FLAT,font='Ivy 25 bold',fg=cor3, padx=0,pady=0,anchor=NW)
labels_meses.place(x=140,y=135)

labels_meses_nome = Label(frame_corpo, text="Meses", width=10 , height= 1 ,bg=cor2, relief=FLAT,font='Ivy 25 bold',fg=cor3, padx=0,pady=0,anchor=NW)
labels_meses_nome.place(x=110,y=180)

labels_dias = Label(frame_corpo, text="--", width=10 , height= 1 ,bg=cor2, relief=FLAT,font='Ivy 25 bold',fg=cor3, padx=0,pady=0,anchor=NW)
labels_dias.place(x=240,y=135)

labels_dias_nome = Label(frame_corpo, text="Dias", width=10 , height= 1 ,bg=cor2, relief=FLAT,font='Ivy 25 bold',fg=cor3, padx=0,pady=0,anchor=NW)
labels_dias_nome.place(x=220,y=180)

#botao calcular

b_1 = Button(frame_corpo,command=calcular,text='CALCULAR', width=10,height=1,relief='raised',font='Ivy 11 bold',overrelief='ridge',bg=cor2,fg=cor3)
b_1.place(x=170,y=80)



janela.mainloop()

